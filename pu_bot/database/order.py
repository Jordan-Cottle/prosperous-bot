"""Module for handling database operations relating to orders."""

from typing import List
from uuid import UUID
import lightbulb

from sqlmodel import select
from pu_bot.database.session import DatabaseSession
from pu_bot.models.order import Order, OrderItem
from pu_bot.utils import get_username


async def create_order(
    session: DatabaseSession,
    item: str,
    amount: int,
    price: int,
    currency: str,
    location: str,
    ctx: lightbulb.Context,
):
    order = Order(
        channel_id=ctx.channel_id,
        guild_id=ctx.guild_id,
        creator_id=ctx.author.id,
        creator_name=await get_username(ctx.guild_id, ctx.user.id),
        items=[
            OrderItem(
                item=item,
                amount=amount,
                price_per_item=price,
                currency=currency,
                location=location,
            )
        ],
    )

    session.add(order)

    return order


def get_order(session: DatabaseSession, order_id: UUID) -> Order:
    """Get an order from the database"""

    return session.exec(select(Order).filter_by(order_id=order_id)).one()


def get_orders(session: DatabaseSession) -> List[Order]:
    """Get orders from the database"""

    return session.exec(select(Order)).all()
