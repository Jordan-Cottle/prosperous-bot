"""Module for low level database operations interacting with projects."""

from typing import List, Optional
from uuid import UUID

from hikari import Snowflake
from sqlmodel import Session, select

from pu_bot.models.project import ItemRequest, Project, ProjectContribution, ProjectItem


def create_project(
    session: Session,
    name: str,
    location: str,
    items: List[ItemRequest],
    creator_id: Snowflake,
    creator_name: str,
) -> Project:
    """Create a new project."""

    project = Project(
        name=name,
        preferred_location=location,
        creator_id=creator_id,
        creator_name=creator_name,
    )
    project.items = [
        ProjectItem.from_orm(item, update={"project_id": project.project_id})
        for item in items
    ]

    session.add(project)

    return project


def get_project(session: Session, project_id: UUID):
    """Get a project from the database."""

    query = select(Project).filter_by(project_id=project_id)
    return session.exec(query).one()


def get_projects(session: Session, include_finished: bool = False) -> List[Project]:
    """Get all projects from the database.

    By default will filter all completed projects out.
    """

    query = select(Project)

    if not include_finished:
        query = query.filter_by(completed_on=None)

    return session.exec(query).all()


def create_project_contribution(
    session: Session,
    project_item: ProjectItem,
    contributor_id: Snowflake,
    contributor_name: str,
    amount: int,
    price_per_item: float,
    currency: str,
    accepted: Optional[bool] = None,
) -> ProjectContribution:
    """Create a new project contribution"""

    project_contribution = ProjectContribution(
        project_item_id=project_item.project_item_id,
        contributor_id=contributor_id,
        contributor_name=contributor_name,
        amount=amount,
        price_per_item=price_per_item,
        currency=currency,
        accepted=accepted,
    )

    session.add(project_contribution)

    return project_contribution


def get_user_project_contributions(
    session: Session, project_id: UUID, contributor_id: Snowflake
) -> List[ProjectContribution]:
    """Get a specific user's contributions to a project."""

    query = (
        select(ProjectContribution)
        .filter_by(contributor_id=contributor_id)
        .join(ProjectItem)
        .join(Project)
        .filter(Project.project_id == project_id)
    )

    return session.exec(query).all()
