"""Database logic for the shipping models."""


from datetime import datetime
from typing import Any, List, Optional

import lightbulb
import miru
from sqlmodel import select

from pu_bot.utils import get_username

from ..models.shipping import OpenOfferShipment, OpenShippingCapacityOffer
from .session import DatabaseSession


async def create_open_shipping_offer(
    session: DatabaseSession,
    origin: str,
    destination: str,
    total_weight_available: float,
    total_volume_available: float,
    shipping_rate: float,
    currency: str,
    ctx: lightbulb.Context,
    launches_at: Optional[datetime] = None,
) -> OpenShippingCapacityOffer:
    """Create a new open shipping offer."""

    assert ctx.guild_id is not None

    offer = OpenShippingCapacityOffer(
        origin=origin,
        destination=destination,
        total_volume_available=total_volume_available,
        total_weight_available=total_weight_available,
        shipping_rate=shipping_rate,
        currency=currency,
        launches_at=launches_at,
        creator_id=ctx.user.id,
        creator_name=await get_username(ctx.guild_id, ctx.user.id),
        guild_id=ctx.guild_id,
    )

    session.add(offer)

    return offer


def get_open_shipping_offer(
    session: DatabaseSession, **kwargs: Any
) -> OpenShippingCapacityOffer:
    """Retrieve an existing shipping offer."""

    query = select(OpenShippingCapacityOffer)

    if kwargs:
        query = query.filter_by(**kwargs)

    return session.exec(query).one()


def get_open_shipping_offers(
    session: DatabaseSession, **kwargs: Any
) -> List[OpenShippingCapacityOffer]:
    """Retrieve an existing shipping offer."""

    query = select(OpenShippingCapacityOffer)

    if kwargs:
        query = query.filter_by(**kwargs)

    return session.exec(query).all()


async def create_open_shipping_shipment(
    session: DatabaseSession,
    offer: OpenShippingCapacityOffer,
    ctx: miru.Context,
    item: str,
    amount: int,
) -> OpenOfferShipment:
    """Create a new open offer shipment."""

    shipment = OpenOfferShipment(
        open_offer=offer,
        user_id=ctx.user.id,
        user_name=await get_username(ctx.guild_id, ctx.user.id),
        item=item,
        amount=amount,
    )

    session.add(shipment)

    return shipment
