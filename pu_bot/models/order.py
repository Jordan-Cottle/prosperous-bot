"""Models for tracking orders."""

from datetime import datetime
from typing import List, Optional
from uuid import UUID, uuid4

from hikari import Snowflake
from sqlmodel import Field, Relationship, SQLModel


class Order(SQLModel, table=True):
    """An order for a collection of items"""

    order_id: UUID = Field(default_factory=uuid4, primary_key=True)

    message_id: Optional[Snowflake]
    channel_id: Optional[Snowflake]
    guild_id: Snowflake

    creator_id: Snowflake
    creator_name: str

    created_on: datetime = Field(default_factory=datetime.now)
    complete: bool = Field(default=False, index=True)

    items: List["OrderItem"] = Relationship(back_populates="order")


class OrderItem(SQLModel, table=True):
    """A specific item requested in an order."""

    order_item_id: UUID = Field(default_factory=uuid4, primary_key=True)
    order_id: UUID = Field(foreign_key="order.order_id")

    item: str
    amount: int
    price_per_item: float

    location: str
    currency: str

    order: Order = Relationship(back_populates="items")
    fulfillers: List["OrderItemFulfiller"] = Relationship(back_populates="order_item")

    @property
    def fulfilled(self) -> bool:
        """Returns true if all ordered items are covered byu fulfillers"""

        return self.amount_remaining <= 0

    @property
    def amount_remaining(self) -> int:
        """Get remaining unfulfilled amount"""

        return self.amount - sum(fulfiller.amount for fulfiller in self.fulfillers)


class OrderItemFulfiller(SQLModel, table=True):
    """A registration from someone who can fulfil all or part of an order"""

    order_item_id: UUID = Field(foreign_key="orderitem.order_item_id", primary_key=True)

    user_id: Snowflake = Field(primary_key=True)
    user_name: str

    amount: int

    order_item: OrderItem = Relationship(back_populates="fulfillers")

    @property
    def total_price(self) -> float:
        """Calculate total price awarded for this fulfillment."""

        return self.amount * self.order_item.price_per_item
