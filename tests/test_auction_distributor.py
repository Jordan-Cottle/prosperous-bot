""""Test module for the auction_distributor.py module"""

from unittest.mock import MagicMock

import pytest
from hikari import Snowflake

from pu_bot.auction_distributor import Auction, Bid, calculate_auction_results


@pytest.mark.parametrize(
    "auction_amount, bids, expected_results",
    [
        (10000, [2000, 2000, 10000], [2000, 2000, 6000]),
        (800, [200, 800, 800], [200, 300, 300]),
        (125, [50, 125, 40], [42, 42, 40]),
        (610, [100], [100]),
        (6900, [6900, 6900, 300, 6900, 6900], [1650, 1650, 300, 1650, 1650]),
        (150, [150, 150], [75, 75]),
        (70, [25], [25]),
        (250, [25], [25]),
        (1000, [1000, 200, 1000], [400, 200, 400]),
        (3405, [3405, 3405, 3405, 3405], [851, 851, 851, 851]),
        (300, [300, 300, 300, 300], [75, 75, 75, 75]),
        (300, [], []),
        (5, [5, 5, 5, 5, 5, 5], [0, 0, 0, 0, 0, 0]),
        (
            30000,
            [30000, 30000, 10000, 30000, 1000, 4000, 30000],
            [5000, 5000, 5000, 5000, 1000, 4000, 5000],
        ),
    ],
)
def test_auction_results(auction_amount, bids, expected_results):
    # Set up auction
    auction = Auction(
        message_id=None,
        channel_id=None,
        guild_id=MagicMock(spec=Snowflake),
        creator_id=MagicMock(spec=Snowflake),
        creator_name="Test auctioneer",
        item="Test",
        amount=auction_amount,
        price_per_item=1,
        location="TST",
        currency="tst",
    )

    auction_bids = [
        Bid(
            user_id=i,
            user_name=f"test-{i}",
            auction_id=auction.auction_id,
            max_amount=amount,
        )
        for i, amount in enumerate(bids)
    ]

    auction.bids = auction_bids

    results = calculate_auction_results(auction.creator_id, auction)

    assert list(results.values()) == expected_results
