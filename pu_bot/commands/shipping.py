"""Module for tracking shipping commands."""

import logging
from datetime import datetime, timedelta
from typing import Optional
from uuid import UUID

import hikari
import lightbulb
import miru
from hikari import Message

from .. import bot
from ..database.session import DatabaseSession
from ..database.shipping import (
    create_open_shipping_offer,
    create_open_shipping_shipment,
    get_open_shipping_offer,
    get_open_shipping_offers,
)
from ..interfaces.fio import get_materials
from ..models.shipping import OpenShippingCapacityOffer

LOGGER = logging.getLogger(__name__)


class AddShipmentModal(miru.Modal):
    """Modal to ask what items and how many are to be shipped."""

    item = miru.TextInput(
        label="Item to ship",
        placeholder=("What item to do you want shipped?"),
        required=True,
    )
    amount = miru.TextInput(
        label="Amount of item to ship ",
        placeholder=("How much of the item do you want shipped?"),
        required=True,
    )

    view: "OpenShippingView"

    def __init__(self, view: "OpenShippingView") -> None:
        super().__init__(title="Shipment Order Form")

        self.view = view

    async def callback(self, context: miru.Context) -> None:
        """Register shipment with offer."""

        free_weight = self.view.offer.free_weight
        free_volume = self.view.offer.free_volume

        requested_item = self.item.value
        assert requested_item is not None

        assert self.amount.value is not None
        try:
            requested_amount = int(self.amount.value)
        except TypeError:
            await context.respond(
                content="Invalid amount submitted", flags=hikari.MessageFlag.EPHEMERAL
            )
            return

        materials = await get_materials()
        try:
            material = materials[requested_item]
        except KeyError:
            await context.respond(
                content=f"Unknown material selected: {requested_item}",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        item_weight = material.weight
        item_volume = material.volume

        requested_weight = requested_amount * item_weight
        requested_volume = requested_amount * item_volume

        valid_request = True
        if requested_volume > free_volume:
            max_items = int(free_volume / item_volume)
            await context.respond(
                content=f"Request too large: only {max_items} {requested_item} will fit into {free_volume}m³",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            valid_request = False

        if requested_weight > free_weight:
            max_items = int(free_weight / item_weight)
            await context.respond(
                content=f"Request too large: only {max_items} {requested_item} will fit into {free_weight}t",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            valid_request = False

        if not valid_request:
            LOGGER.info(
                f"Rejected invalid shipment of {requested_amount} {requested_item} on "
                f"offer {self.view.offer.open_shipping_capacity_offer_id}"
            )
            return

        shipment = await create_open_shipping_shipment(
            self.view.session,
            self.view.offer,
            context,
            requested_item,
            requested_amount,
        )
        self.view.session.commit()
        LOGGER.info(
            f"Shipment {shipment.shipment_id} created "
            f"for offer {shipment.open_shipping_capacity_offer_id}"
        )
        await self.view.update_message()

        await context.respond(
            content=f"Shipment of {requested_amount} {requested_item} added to shipping offer",
            flags=hikari.MessageFlag.EPHEMERAL,
        )


class AddShipmentButton(miru.Button):
    """Button that allows for adding a shipment to a shipping offer"""

    view: "OpenShippingView"

    def __init__(self, offer_id: str):
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Add Shipment",
            custom_id="add_shipment_button_" + str(offer_id),
        )

    async def callback(self, context: miru.ViewContext) -> None:
        """Finish out the offer attached to this view."""

        modal = AddShipmentModal(self.view)

        await context.respond_with_modal(modal)


class FinishShippingOfferButton(miru.Button):
    """Button that allows for completing a shipping offer"""

    view: "OpenShippingView"

    def __init__(self, offer_id: str):
        super().__init__(
            style=hikari.ButtonStyle.SUCCESS,
            label="Finish",
            custom_id="finish_shipping_offer_button_" + str(offer_id),
        )

    async def callback(self, context: miru.ViewContext) -> None:
        """Finish out the offer attached to this view."""

        if self.view.offer.creator_id != context.user.id:
            await context.respond(
                content="Only the creator of an auction can finish it",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        LOGGER.info(
            f"Closing shipping offer {self.view.offer.open_shipping_capacity_offer_id}"
        )

        self.view.offer.finished_at = datetime.now()
        self.view.session.commit()
        await self.view.update_message()


class OpenShippingView(miru.View):
    """An open instance of an open shipping capacity offer."""

    @classmethod
    async def new(
        cls,
        origin: str,
        destination: str,
        total_weight_available: float,
        total_volume_available: float,
        shipping_rate: float,
        currency: str,
        ctx: lightbulb.Context,
        launches_at: Optional[datetime] = None,
    ) -> "OpenShippingView":
        """Create a new OpenShippingView, ensuring that it is persisted into the database.

        This constructor also handles creating the initial response, sending it, starting the view,
        and linking that message to the offer in the database.

        This way, it is impossible to end up with an OpenShippingView that isn't persisted or linked
        to the message properly.
        """

        session = DatabaseSession()
        offer = await create_open_shipping_offer(
            session,
            origin,
            destination,
            total_weight_available,
            total_volume_available,
            shipping_rate,
            currency,
            ctx,
            launches_at=launches_at,
        )
        session.commit()

        view = cls(offer.open_shipping_capacity_offer_id)

        response = await ctx.respond(view.generate_message(), components=view)
        message = await response.message()

        await view.start(message)

        offer.channel_id = message.channel_id
        offer.message_id = message.id
        session.commit()

        return view

    def __init__(
        self,
        open_shipping_capacity_offer_id: UUID,
        session: Optional[DatabaseSession] = None,
    ) -> None:
        super().__init__(timeout=None)

        self.session = session or DatabaseSession()
        self.offer: OpenShippingCapacityOffer = get_open_shipping_offer(
            self.session,
            open_shipping_capacity_offer_id=open_shipping_capacity_offer_id,
        )

        self.add_item(
            AddShipmentButton(str(self.offer.open_shipping_capacity_offer_id))
        )
        self.add_item(
            FinishShippingOfferButton(str(self.offer.open_shipping_capacity_offer_id))
        )

    def generate_message(self) -> str:
        """Generate the message for the view."""

        if self.offer.finished_at:
            launches_at = f"<t:{int(self.offer.finished_at.timestamp())}:R>"
        elif self.offer.launches_at:
            launches_at = f"<t:{int(self.offer.launches_at.timestamp())}:R>"
        else:
            launches_at = "soon™"

        header = "\n".join(
            [
                " | ".join(
                    [
                        f"**{self.offer.origin} -> {self.offer.destination}**",
                        f"{self.offer.total_weight_available:,.2f}t/{self.offer.total_volume_available:,.2f}m³",
                        f"departing {launches_at}",
                    ]
                ),
                "Post ad on LM and set **autoprovision**",
                f"{self.offer.shipping_rate:,.2f} {self.offer.currency} per t/m³",
            ]
        )

        shipments = []

        for shipment in self.offer.shipments:
            shipments.append(
                f"* {shipment.user_name} "
                f"- {shipment.weight:,.2f}t/{shipment.volume:,.2f}m³ "
                f"- {shipment.price:,.2f} {self.offer.currency}"
            )

        return "\n".join(
            [
                header,
                "\n".join(shipments),
                f"Unallocated space: {self.offer.free_weight:,.2f}t/{self.offer.free_volume:,.2f}m³",
            ]
        )

    async def get_message(self) -> Message:
        """Get the message associated with this view."""

        if self._message is None:
            # Lazy load message if it is not yet loaded
            assert self.offer.channel_id is not None
            assert self.offer.message_id is not None
            self._message = await self.bot.rest.fetch_message(
                self.offer.channel_id, self.offer.message_id
            )

        return self._message

    def stop(self) -> None:
        """Make sure database session is closed when view is stopped."""
        super().stop()
        self.session.close()

    async def update_message(self) -> None:
        """Update the message with the latest state."""

        message = await self.get_message()

        if self.offer.finished_at is not None:
            await message.edit(self.generate_message(), components=[])
            self.stop()
        else:
            await message.edit(self.generate_message())


@bot.command
@lightbulb.option(
    name="origin",
    type=str,
    description="The starting point of the shipment",
    required=True,
)
@lightbulb.option(
    name="destination",
    type=str,
    description="The final location of the shipment",
    required=True,
)
@lightbulb.option(
    name="total_weight_available",
    type=float,
    description="The total tonnage available for shipments",
    required=True,
)
@lightbulb.option(
    name="total_volume_available",
    type=str,
    description="The total volume available for shipments",
    required=True,
)
@lightbulb.option(
    name="launches_in_minutes",
    type=int,
    description="The number of minutes (roughly) you plan of waiting before sending the ship",
    default=None,
    required=False,
)
@lightbulb.option(
    name="shipping_rate",
    type=float,
    description="The currency you want to spend for this order",
    default=10,
    required=False,
)
@lightbulb.option(
    name="currency",
    type=str,
    description="The currency you want to charge for this order",
    default="AIC",
    required=False,
)
@lightbulb.command("shipping-offer", "Create a new offer to ship goods")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_new_order(ctx: lightbulb.Context) -> None:
    """Create a new order to find people who can fulfill it"""

    view = await OpenShippingView.new(
        ctx.options.origin,
        ctx.options.destination,
        ctx.options.total_weight_available,
        ctx.options.total_volume_available,
        ctx.options.shipping_rate,
        ctx.options.currency,
        ctx,
        launches_at=ctx.options.launches_in_minutes
        and (datetime.now() + timedelta(minutes=ctx.options.launches_in_minutes)),
    )

    LOGGER.info(
        f"New shipping offer created: {view.offer.open_shipping_capacity_offer_id}"
    )

    await view.wait()


@bot.listen()
async def reload_shipping_offers(_event: hikari.StartedEvent) -> None:
    """Reload shipping offers from the database."""

    with DatabaseSession() as session:
        for offer in get_open_shipping_offers(session):
            view = OpenShippingView(
                open_shipping_capacity_offer_id=offer.open_shipping_capacity_offer_id
            )
            await view.start(offer.message_id)


@bot.listen()
async def load_material_cache(_event: hikari.StartedEvent) -> None:
    """Load materials from FIO on startup."""

    await get_materials()
