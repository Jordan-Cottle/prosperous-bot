""" Register commands and views for the bot. """

import logging
from typing import Dict, Optional
from uuid import UUID

import hikari
import lightbulb
import miru
from miru.ext.nav import NavigatorView
from hikari import Message, Snowflake

from pu_bot.auction_distributor import (
    Auction,
    DuplicateBid,
    complete_auction,
    create_bid,
    remove_bid,
)
from pu_bot.database.auction import get_auction, get_auctions, get_bid
from pu_bot.database.session import DatabaseSession
from pu_bot.models.auction import Bid
from pu_bot.utils import get_username

from ..bot import bot

LOGGER = logging.getLogger(__name__)

AUCTION_VIEWS: Dict[UUID, "AuctionView"] = {}


class BidAllButton(miru.Button):
    """The button that triggers a new bid for up to the maximum amount."""

    view: "AuctionView"

    def __init__(self, auction_id: str):
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Maximum Bid",
            custom_id=f"bid_button_{auction_id}",
        )

    async def callback(self, context: miru.ViewContext) -> None:
        """Create a new bid and update the message."""
        assert context.guild_id is not None
        try:
            bid = create_bid(
                self.view.session,
                context.user.id,
                await get_username(context.guild_id, context.user.id),
                self.view.auction,
            )
            self.view.session.add(bid)
        except (
            DuplicateBid
        ):  # If user already registered as bidder, remove them from the auction
            bid = get_bid(
                self.view.session, self.view.auction.auction_id, context.user.id
            )
            assert bid is not None

            # If bid is already for max amount, remove it
            if bid.max_amount is None or bid.max_amount >= self.view.auction.amount:
                remove_bid(self.view.session, context.user.id, self.view.auction)
            else:  # Otherwise, set it to max amount
                bid.max_amount = self.view.auction.amount
        except ValueError as error:
            await context.respond(
                content=str(error), flags=hikari.MessageFlag.EPHEMERAL
            )
            return

        self.view.session.commit()
        await self.view.update_active_message()


class PartialBidButton(miru.Button):
    """Allows for bidding up to a maximum amount."""

    view: "AuctionView"

    def __init__(self, auction_id: str):
        LOGGER.debug(f"Setting up partial bid for auction '{auction_id}'")
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Limited Bid",
            custom_id=f"change_bid_{auction_id}",
        )

    async def callback(self, context: miru.ViewContext) -> None:
        modal = LimitedBidModal(view=self.view)
        await context.respond_with_modal(modal)


class FinishButton(miru.Button):
    """Button that allows for completing an auction and calculating the results."""

    view: "AuctionView"

    def __init__(self, auction_id: str):
        super().__init__(
            style=hikari.ButtonStyle.SUCCESS,
            label="Finish",
            custom_id=f"finish_button_{auction_id}",
        )

    async def callback(self, context: miru.ViewContext) -> None:
        """Finish out the auction attached to this view."""
        auction = get_auction(self.view.session, self.view.auction.auction_id)
        if context.user.id != auction.creator_id:
            await context.respond(
                content="Only the creator of an auction can finish it",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        await self.view.complete_auction()


class LimitedBidModal(miru.Modal):
    """Modal that asks for an amount to bid rather than using the full amount."""

    max_amount = miru.TextInput(
        label="Max Amount",
        placeholder=("Max amount you'd like to receive"),
        required=True,
    )

    def __init__(self, view: "AuctionView"):
        super().__init__(
            title="Limited Bid Amount",
            custom_id=f"limited_bid_amount_{view.auction.auction_id}",
        )
        self.view = view

        # Set placeholder to the amount the max
        self.max_amount.placeholder = str(self.view.auction.amount)

    def get_bid(self, ctx: miru.ModalContext) -> Optional[Bid]:
        """Get a bid using the ctx and current view's session"""
        return get_bid(self.view.session, self.view.auction.auction_id, ctx.user.id)

    # The callback function is called after the user hits 'Submit'
    async def callback(self, ctx: miru.ModalContext) -> None:
        if self.max_amount.value is None or not self.max_amount.value.isdigit():
            await ctx.respond(
                content=(f"Please enter number."),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        max_amount = int(self.max_amount.value)
        bid = self.get_bid(ctx)

        if bid:
            bid.max_amount = max_amount
        else:
            assert ctx.guild_id is not None
            bid = create_bid(
                self.view.session,
                ctx.user.id,
                await get_username(ctx.guild_id, ctx.user.id),
                self.view.auction,
                max_amount,
            )
        self.view.session.commit()

        await self.view.update_active_message()
        await ctx.respond(
            content=(f"Bid request maximum amount set to {max_amount:,d}"),
            flags=hikari.MessageFlag.EPHEMERAL,
        )


class AuctionView(miru.View):
    """A view for the context."""

    @classmethod
    async def new(
        cls,
        guild_id: Snowflake,
        user_id: Snowflake,
        user_name: str,
        item: str,
        amount: int,
        price: float,
        location: str,
        currency: str,
        ctx: lightbulb.Context,
    ) -> "AuctionView":
        """Create a new auction, ensuring data is persisted to the database"""

        auction = Auction(
            guild_id=guild_id,
            creator_id=user_id,
            creator_name=user_name,
            item=item,
            amount=amount,
            price_per_item=price,
            location=location,
            currency=currency,
            message_id=None,
            channel_id=None,
        )

        session = DatabaseSession()
        session.add(auction)
        session.commit()

        view = cls(auction.auction_id)

        response = await ctx.respond(
            await view.update_active_message(False),
            components=view,
        )
        message = await response.message()

        await view.start(message)

        auction.message_id = message.id
        auction.channel_id = message.channel_id
        session.commit()
        # Refresh the view's model since it will have cached the initial None values
        view.session.refresh(view.auction)

        return view

    def __init__(
        self, auction_id: UUID, session: Optional[DatabaseSession] = None
    ) -> None:
        super().__init__(timeout=None)

        self.session = session or DatabaseSession()
        self.auction = get_auction(self.session, auction_id)

        self.add_item(BidAllButton(str(auction_id)))
        self.add_item(PartialBidButton(str(auction_id)))
        self.add_item(FinishButton(str(auction_id)))

        self.cached_message: Optional[Message] = None

        AUCTION_VIEWS[self.auction.auction_id] = self

    def stop(self) -> None:
        """Make sure database session is closed when view is stopped."""
        super().stop()
        AUCTION_VIEWS.pop(self.auction.auction_id, None)
        self.session.close()

    async def update_active_message(self, message_owned: bool = True) -> str:
        """Update the message on the view to contain most recent information."""

        auction = self.auction
        new_message = (
            f"{auction.amount:,d} {auction.item}@{auction.price_per_item:,.2f} "
            f"{auction.currency}/u available on {auction.location}"
        )
        if len(auction.bids) > 0:
            new_message += "\nBids:"
        for bid in auction.bids:
            max_amount = bid.max_amount or auction.amount
            new_message += f"\n* {bid.user_name}: Up to {max_amount:,d}"

        if message_owned:
            if self.cached_message is None:
                assert auction.channel_id is not None
                assert auction.message_id is not None
                self.cached_message = await self.app.rest.fetch_message(
                    auction.channel_id, auction.message_id
                )

            await self.cached_message.edit(content=new_message)
            self.cached_message.content = new_message

        return new_message

    async def complete_auction(self) -> None:
        """Close out the auction and display the results."""
        message = complete_auction(self.auction)

        if self.cached_message is None:
            assert self.auction.channel_id is not None
            assert self.auction.message_id is not None
            self.cached_message = await self.app.rest.fetch_message(
                self.auction.channel_id, self.auction.message_id
            )
        self.session.commit()

        await self.cached_message.edit(content=message, components=[])
        self.stop()

    async def on_timeout(self) -> None:
        """Close out the auction after a period of inactivity"""

        await self.complete_auction()


@bot.listen()
async def startup_views(_event: hikari.StartedEvent) -> None:
    """Load auctions from persistence on startup."""
    with DatabaseSession() as session:
        for auction in get_auctions(session):
            view = AuctionView(auction.auction_id)
            if auction.channel_id is None or auction.message_id is None:
                LOGGER.warning(
                    f"Unable to reload {auction} due to missing channel and/or message ids"
                )
                continue
            await view.start(auction.message_id)


@bot.command
@lightbulb.command("ping", "Check if the bot is alive")
@lightbulb.implements(lightbulb.SlashCommand)
async def ping(ctx: lightbulb.Context) -> None:
    """Simple ping handler to test bot functionality"""

    await ctx.respond("Pong!")


@bot.command
@lightbulb.option(
    name="item",
    type=str,
    description="The item you want to create an auction for",
    required=True,
)
@lightbulb.option(
    name="amount",
    type=int,
    description="The amount of items you want to auction off",
    required=True,
)
@lightbulb.option(
    name="price",
    type=float,
    description="The price of each item you want to auction off",
    required=True,
)
@lightbulb.option(
    name="location",
    type=str,
    description="The location of the auction.",
    required=True,
)
@lightbulb.option(
    name="currency",
    type=str,
    description="The currency to use for the auction.",
    default="AIC",
    required=False,
)
@lightbulb.command("auction", "Start a new auction")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_new_auction(ctx: lightbulb.Context) -> None:
    """Create a new auction for an item."""

    assert (
        ctx.guild_id is not None
    ), "Auctions should only be created in the context of a guild"

    view = await AuctionView.new(
        ctx=ctx,
        guild_id=ctx.guild_id,
        user_id=ctx.user.id,
        user_name=await get_username(ctx.guild_id, ctx.user.id),
        item=ctx.options.item,
        amount=ctx.options.amount,
        price=ctx.options.price,
        location=ctx.options.location,
        currency=ctx.options.currency,
    )

    LOGGER.info(f"New auction created: {view.auction.auction_id}")

    await view.wait()


def create_message_link(
    guild_id: Snowflake, channel_id: Snowflake, msg_id: Snowflake
) -> str:
    return f"https://discord.com/channels/{guild_id}/{channel_id}/{msg_id}"


@bot.command
@lightbulb.command("list_auctions", "List all active auctions in the current server")
@lightbulb.implements(lightbulb.SlashCommand)
async def list_all_active_auction(ctx: lightbulb.Context) -> None:
    """List all available auctions"""
    assert (
        ctx.guild_id is not None
    ), "Auctions should only be listed in the context of a guild"

    pages = []

    with DatabaseSession() as session:
        page = []
        for auction in sorted(
            get_auctions(session),
            key=lambda auction: auction.message_id or 0,
            reverse=True,
        ):
            if len(page) == 10:
                pages.append("\n".join(page))
                page = []

            if auction.guild_id != ctx.guild_id:
                continue

            if auction.channel_id is None:
                LOGGER.debug(f"Skipping {auction} with no channel id")
                continue

            if auction.message_id is None:
                LOGGER.debug(f"Skipping {auction} with no message id")
                continue

            link = create_message_link(
                auction.guild_id, auction.channel_id, auction.message_id
            )
            page.append(
                f"* {auction.amount:,d} {auction.item}@{auction.price_per_item:,.2f} "
                f"{auction.currency}/u from {auction.creator_name} on {auction.location}: {link}"
            )

    if page:
        pages.append("\n".join(page))

    if not pages:
        await ctx.respond("No active auctions.")
        return

    navigator = NavigatorView(pages=pages, timeout=200000)
    await navigator.send(ctx.interaction)
    await navigator.wait()
