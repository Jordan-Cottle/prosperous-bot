"""Run the bot."""

import asyncio
from pu_bot.auction_watcher import audit_auctions_forever
from pu_bot.bot import bot

import pu_bot.commands  # pylint: disable=unused-import


async def main():
    await bot.start()

    tasks = [
        audit_auctions_forever(),
        bot.join(),
    ]

    await asyncio.gather(*tasks)


asyncio.run(main())
