"""Module that defines sql models to use with auctions."""

from datetime import datetime
from typing import List, Optional
from uuid import UUID, uuid4

from hikari import Snowflake
from sqlmodel import Field, Relationship, SQLModel


class Auction(SQLModel, table=True):
    """Tracks a specific instance of an auction."""

    auction_id: UUID = Field(default_factory=uuid4, primary_key=True)
    message_id: Optional[Snowflake]
    channel_id: Optional[Snowflake]
    guild_id: Snowflake
    creator_id: Snowflake
    creator_name: str
    item: str
    amount: int
    price_per_item: float
    location: str
    currency: str

    max_duration_days: int = Field(default=7)
    complete: bool = Field(default=False, index=True)

    bids: List["Bid"] = Relationship(back_populates="auction")

    @property
    def created_at(self) -> Optional[datetime]:
        """Get the time the auction was created.

        Returns None if there is no discord message id.
        """

        if self.message_id is None:
            return None

        snowflake = Snowflake(self.message_id)

        return snowflake.created_at

    def __str__(self) -> str:
        string = (
            f"{'Completed' if self.complete else 'Open'} auction for {self.amount:,d} "
            f"{self.item} for {self.price_per_item:.2f} {self.currency} each on {self.location} "
            f"by {self.creator_name}"
        )
        return string


class Bid(SQLModel, table=True):
    """Tracks a specific bid on an auction"""

    auction_id: UUID = Field(foreign_key="auction.auction_id", primary_key=True)
    user_id: Snowflake = Field(primary_key=True)
    user_name: str
    max_amount: Optional[int]

    auction: Auction = Relationship(back_populates="bids")

    def __str__(self) -> str:
        return f"{self.user_name} - {self.max_amount or self.auction.amount:,d}"
