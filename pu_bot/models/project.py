"""Models for managing large projects."""

from datetime import datetime
import re
from typing import List, Optional
from uuid import UUID, uuid4
from hikari import Snowflake
from sqlmodel import Field, Relationship, SQLModel


class ProjectTemplate(SQLModel, table=True):
    """A template for a project

    These make it easier to set up a common project like a standard ship build or a common base
    without needing to remember all of the different materials required.
    """

    project_template_id: UUID = Field(default_factory=uuid4, primary_key=True)
    name: str
    preferred_location: str

    guild_id: Snowflake

    items: List["ProjectTemplateItem"] = Relationship(back_populates="project_template")


class ProjectTemplateItem(SQLModel, table=True):
    """An item that belongs to a template"""

    project_template_item_id: UUID = Field(default_factory=uuid4, primary_key=True)
    project_template_id: UUID = Field(
        foreign_key="projecttemplate.project_template_id", primary_key=True
    )
    name: str
    total_amount: int
    price_per_item: Optional[float] = None
    preferred_currency: Optional[str] = None

    project_template: ProjectTemplate = Relationship(back_populates="items")


class Project(SQLModel, table=True):
    """An instance of a project being worked on."""

    project_id: UUID = Field(default_factory=uuid4, primary_key=True)
    name: str
    preferred_location: str

    guild_id: Optional[Snowflake] = None
    message_id: Optional[Snowflake] = None
    channel_id: Optional[Snowflake] = None

    creator_id: Snowflake
    creator_name: str

    created_on: datetime = Field(default_factory=datetime.now)
    completed_on: Optional[datetime] = None

    items: List["ProjectItem"] = Relationship(back_populates="project")

    @property
    def completed(self) -> bool:
        """Check wether this project has been completed or not"""

        return self.completed_on is not None

    @property
    def contributions(self) -> List["ProjectContribution"]:
        """Get all contributions for the project"""

        contributions = []
        for item in self.items:
            contributions.extend(item.contributions)

        return contributions

    def __str__(self) -> str:
        return f"{self.creator_name}'s {self.name} on {self.preferred_location}"


class ItemRequest(SQLModel):
    """Request for an amount of an item, optionally for a specific price"""

    name: str
    total_amount: int
    price_per_item: Optional[float] = None
    preferred_currency: Optional[str] = None

    @classmethod
    def parse_input(cls, string: str) -> "Self":
        """Parse fields out of an input string

        Format <amount>x<item>@<price><currency>/u

        Price and currency are optional.
        """

        match = re.match(
            r"(\d+)[x ]*([a-zA-z]\w*)[@ ]*(\d+\.?\d+?)?[ ]*(\w+)?/?u?", string.strip()
        )

        if not match:
            raise ValueError(f"Unable to parse '{string}' as a valid item request")

        amount, item, price, currency = match.groups()

        return cls(
            name=item,
            total_amount=amount,
            price_per_item=price,
            preferred_currency=currency,
        )


class ProjectItem(ItemRequest, table=True):
    """An item that is part of a project."""

    project_item_id: UUID = Field(default_factory=uuid4, primary_key=True)
    project_id: UUID = Field(foreign_key="project.project_id")

    project: Project = Relationship(back_populates="items")
    contributions: List["ProjectContribution"] = Relationship(
        back_populates="project_item"
    )

    @property
    def accepted_contributions(self) -> List["ProjectContribution"]:
        """Get list of all accepted contributions"""

        return [
            contribution for contribution in self.contributions if contribution.accepted
        ]

    @property
    def remaining_items_needed(self) -> int:
        """Calculate the amount of items needed still based on accepted contributions."""

        contributed_items = sum(
            contribution.amount for contribution in self.accepted_contributions
        )

        return self.total_amount - contributed_items

    def __str__(self) -> str:
        if self.price_per_item:
            price = f"{self.price_per_item:,.2f}"
        else:
            price = "any price"

        if self.preferred_currency:
            currency = self.preferred_currency
        else:
            currency = "in any currency"

        return f"{self.remaining_items_needed}/{self.total_amount} {self.name} @ {price} {currency}"


class ProjectContribution(SQLModel, table=True):
    """A contribution from a specific player to a project.

    Starts off as an offer if no price has been agreed on, becomes a contract once accepted.

    Contracts will be automatically accepted if they use the default price for an item.
    """

    project_contribution_id: UUID = Field(default_factory=uuid4, primary_key=True)
    project_item_id: UUID = Field(foreign_key="projectitem.project_item_id")

    contributor_id: Snowflake
    contributor_name: str

    amount: int
    price_per_item: float
    currency: str

    review_channel_id: Optional[Snowflake] = None
    review_message_id: Optional[Snowflake] = None
    is_counter_offer: bool = False

    accepted: Optional[bool] = None

    project_item: ProjectItem = Relationship(back_populates="contributions")

    def __str__(self) -> str:
        total_cost = self.price_per_item * self.amount
        return (
            f"{self.contributor_name}: {self.amount} {self.project_item.name} "
            f"for a total of {total_cost:,.2f} {self.currency} ({self.price_per_item:,.2f}/u)"
        )

    @property
    def reviewer_id(self) -> Snowflake:
        """Get the id of the owner of the project who will review this contribution request"""

        return self.project_item.project.creator_id
