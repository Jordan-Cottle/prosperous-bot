"""Module for commands that allow for managing larger projects."""

# Create project

from asyncio import sleep
import logging
from datetime import datetime
from operator import attrgetter
from typing import Any, Iterable, List, Union
from uuid import UUID

import hikari
import lightbulb
import miru
from hikari import undefined, messages
from hikari.errors import ForbiddenError
from miru.ext.nav import NavButton, NavigatorView, NavTextSelect, Page
from pydantic import ValidationError
from sqlmodel import Session

from pu_bot import bot
from pu_bot.database.project import (
    create_project,
    create_project_contribution,
    get_project,
    get_projects,
    get_user_project_contributions,
)
from pu_bot.database.session import DatabaseSession
from pu_bot.models.project import ItemRequest, Project, ProjectContribution, ProjectItem
from pu_bot.utils import get_username

LOGGER = logging.getLogger(__name__)

CONTRIBUTION_STATUSES = {True: ":white_check_mark:", False: ":x:", None: ":question:"}
PROJECT_CLOSE_DELAY = 7 * 24 * 60 * 60  # 7 days


def contribution_sort_key(contribution: ProjectContribution) -> int:
    """Get key to sort contributions by when displaying them to users"""
    return contribution.accepted if contribution.accepted is not None else 2


# Create new projects


class CreateProjectModal(miru.Modal):
    """Modal for setting options to create a project"""

    name = miru.TextInput(
        label="Name", placeholder="Name of your project", required=True
    )
    items = miru.TextInput(
        label="Items",
        placeholder="Ex: 100xRAT@100AIC/u,100xDW@100AIC/u,100xCOF",
        required=True,
    )
    location = miru.TextInput(label="Pickup Location", placeholder="ANT", required=True)

    async def callback(self, context: miru.ModalContext) -> None:
        """Process input and create the project."""
        assert isinstance(self.name.value, str)
        assert isinstance(self.items.value, str)
        assert isinstance(self.location.value, str)
        assert context.guild_id is not None

        items: List[ItemRequest] = []
        for item in self.items.value.split(","):
            try:
                item_request = ItemRequest.parse_input(item)
            except ValueError as error:
                await context.respond(
                    content=(
                        f"{error}\n\nPlease use a format like '100 RAT@25AIC/u' "
                        "where the price and currency are optional"
                    ),
                    flags=hikari.MessageFlag.EPHEMERAL,
                )
                return

            items.append(item_request)

        with DatabaseSession() as session:
            project = create_project(
                session,
                self.name.value,
                self.location.value,
                items,
                context.user.id,
                await get_username(context.guild_id, context.user.id),
            )

            session.commit()

            await ProjectView.new(context, session, project)


class FinishButton(NavButton):
    """Button used to finish a project."""

    view: "ProjectView"

    def __init__(self, project_id: str) -> None:
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Finish",
            custom_id=f"finish_project_{project_id}",
        )

    async def callback(self, context: miru.ViewContext) -> None:
        if context.author.id != self.view.project.creator_id:
            await context.respond(
                f"Only the owner of the project can finish it",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        await self.view.finish()


class MyContributionsButton(NavButton):
    """Button used to list out someone's contributions"""

    view: "ProjectView"

    def __init__(self, project_id: str) -> None:
        super().__init__(
            style=hikari.ButtonStyle.SECONDARY,
            label="My Contributions",
            custom_id=f"list_contributions_{project_id}",
        )

    async def callback(self, context: miru.ViewContext) -> None:
        contributions = get_user_project_contributions(
            self.view.session, self.view.project.project_id, context.user.id
        )

        if not contributions:
            await context.respond(
                "You have no contributions for this project",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        username = contributions[0].contributor_name

        message = [f"{username}'s contributions:"]
        for contribution in sorted(
            contributions,
            key=contribution_sort_key,
            reverse=True,
        ):
            status = CONTRIBUTION_STATUSES[contribution.accepted]

            message.append(f"{status}: {contribution}")

        await context.respond("\n".join(message), flags=hikari.MessageFlag.EPHEMERAL)


class ProjectView(NavigatorView):
    """The view for a project

    Enables other uses to see the list of needed items and contribute to it.
    """

    @classmethod
    async def new(cls, context: miru.ModalContext, session: Session, project: Project):
        """Create a new view, sending the message and persisting the message info"""

        view = cls(project.project_id)

        await view.send(context)

        assert view.message is not None
        project.guild_id = context.guild_id
        project.channel_id = view.message.channel_id
        project.message_id = view.message.id
        session.commit()

        return view

    def __init__(self, project_id: UUID) -> None:
        self.session = DatabaseSession()
        self.project = get_project(self.session, project_id)
        self.context = ProjectContext(self.project)

        buttons = self.get_default_buttons()
        for button in buttons:
            button._custom_id = (
                f"project_{type(button).__qualname__.lower()}_{project_id}"
            )

        super().__init__(pages=self.get_pages(), buttons=buttons, timeout=None)

        self.item_select = ContributionSelect(self.project)
        self.add_item(self.item_select)

        self.finish_button = FinishButton(str(self.project.project_id))
        self.add_item(self.finish_button)

        self.contributions_button = MyContributionsButton(str(self.project.project_id))
        self.add_item(self.contributions_button)

    @property
    def message_link(self) -> str:
        return f"https://discord.com/channels/{self.project.guild_id}/{self.project.channel_id}/{self.project.message_id}"  # pylint: disable=line-too-long

    def stop(self) -> None:
        """Make sure database session is closed when view is stopped."""
        super().stop()
        self.session.close()

    def get_pages(self) -> List[Page]:
        """Generate the pages to include for this view."""

        items = {"required": [], "fulfilled": []}

        for item in self.project.items:
            if item.remaining_items_needed <= 0:
                items["fulfilled"].append(item)
            else:
                items["required"].append(item)

        pages: List[Page] = []

        pages.extend(self.create_pages("Required Items: ", items["required"]))
        pages.extend(self.create_pages("Fulfilled Items: ", items["fulfilled"]))

        return pages

    def create_pages(self, title: str, items: List[ProjectItem]) -> Iterable[Page]:
        """Create pages for items required by the project"""

        lines: List[str] = []
        for item in sorted(items, key=attrgetter("name")):
            lines.append(f"* {item}  ")
            for contribution in sorted(
                item.contributions,
                key=contribution_sort_key,
                reverse=True,
            ):
                lines.append(
                    f"\t{CONTRIBUTION_STATUSES[contribution.accepted]} {contribution}  "
                )

            if len(lines) >= 10:
                yield self.create_page(title, lines)
                lines = []

        if lines:
            yield self.create_page(title, lines)

    def create_page(self, title: str, items: List[str]) -> Page:
        """Create a page out of the items"""

        message = [f"{self.project}", title, *items]

        if self.project.completed:
            message.append("**Complete**")

        return Page(content="\n".join(message))

    async def update_message(self) -> None:
        """Update the message based on new information"""

        if not self.project.completed:
            try:
                self.item_select.update_options()
            except NoValidOptions:
                await self.finish()
                return

        if not self.context.initialized:
            await self.context.init()

        await self.swap_pages(self.context, self.get_pages())

    async def finish(self) -> None:
        """Finish the project, cleaning up the view and marking it as done in the DB."""

        self.project.completed_on = datetime.now()
        self.session.commit()

        self.remove_item(self.item_select)
        self.remove_item(self.finish_button)

        await self.update_message()

        # Don't stop view immediately, let it keep running until project is complete

        await sleep(PROJECT_CLOSE_DELAY)

        self.stop()


@bot.command
@lightbulb.command("project", "Start a new project")
@lightbulb.implements(lightbulb.SlashCommand)
async def create_project_command(ctx: lightbulb.Context) -> None:
    """Create a new project"""

    modal = CreateProjectModal("Create Project", timeout=3600)

    await modal.send(ctx.interaction)
    await modal.wait()


# Contribute to project


class NoValidOptions(Exception):
    """Raised when there are no valid options left"""


class ContributionSelect(NavTextSelect):
    """Modal to prompt for contribution details."""

    view: ProjectView

    def __init__(self, project: Project):
        self.project = project
        super().__init__(
            options=self.get_options(),
            placeholder="Select a material",
            custom_id=f"contribution_select_{self.project.project_id}",
        )

    def get_options(self) -> List[miru.SelectOption]:
        select_options = []
        for item in self.project.items:
            if item.remaining_items_needed <= 0:
                continue

            if item.price_per_item and item.preferred_currency:
                description = f"{item.price_per_item} {item.preferred_currency}/u"
            else:
                description = None

            select_options.append(
                miru.SelectOption(
                    label=f"{item.remaining_items_needed} {item.name}",
                    value=str(item.project_item_id),
                    description=description,
                )
            )

        if not select_options:
            raise NoValidOptions("No valid options left to contribute")

        return select_options

    def update_options(self) -> None:
        self.options = self.get_options()

    async def callback(self, context: miru.ModalContext) -> None:
        """Create contribution offer from modal inputs

        If offer matches request exactly, auto accept it, otherwise prompt for confirmation.
        """

        items = {str(item.project_item_id): item for item in self.project.items}

        for value in self.values:
            item = items[value]

            if item.remaining_items_needed <= 0:
                await context.respond(
                    f"All {item.name} have been fulfilled already",
                    flags=hikari.MessageFlag.EPHEMERAL,
                )
                return

            modal = ContributionModal(item, self.view)
            await modal.send(context.interaction)
            await modal.wait()


class ContributionModal(miru.Modal):
    """Modal to prompt for more into about what to submit"""

    def __init__(self, item: ProjectItem, view: ProjectView) -> None:
        super().__init__(f"Contribute {item.name}")
        self.view = view
        self.item = item

        self.amount = miru.TextInput(
            label="Amount", value=str(item.remaining_items_needed), required=True
        )
        self.price_per_item = miru.TextInput(
            label="Price Per Item",
            value=str(item.price_per_item) if item.price_per_item else None,
            required=True,
        )
        self.currency = miru.TextInput(
            label="Currency", value=item.preferred_currency, required=True
        )

        self.add_item(self.amount)
        self.add_item(self.price_per_item)
        self.add_item(self.currency)

    async def callback(self, context: miru.ModalContext) -> None:
        """Create contribution, auto accept if price matches the offer"""
        assert self.amount.value is not None
        assert self.price_per_item.value is not None
        assert self.currency.value is not None

        try:
            project_contribution = create_project_contribution(
                self.view.session,
                self.item,
                context.user.id,
                await get_username(context.guild_id, context.user.id),
                self.amount.value,
                self.price_per_item.value,
                self.currency.value,
            )
        except ValidationError as error:
            await context.respond(
                content=(str(error)),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        if project_contribution.amount > self.item.remaining_items_needed:
            project_contribution.amount = self.item.remaining_items_needed

        self.view.session.commit()

        if (
            self.item.price_per_item
            and project_contribution.price_per_item == self.item.price_per_item
            and project_contribution.currency == self.item.preferred_currency
        ):
            project_contribution.accepted = True
            await context.respond(
                f"Contribution of {project_contribution} was automatically accepted",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            assert self.view.project.guild_id is not None
            project_manager = await self.app.rest.fetch_member(
                self.view.project.guild_id, self.view.project.creator_id
            )

            try:
                await project_manager.send(
                    f"Contribution of {project_contribution} was automatically accepted "
                    f"for {self.view.message_link}"
                )
            except ForbiddenError as error:
                LOGGER.warning(
                    f"Unable to notify {project_manager.display_name} about {project_contribution}"
                )

        else:
            await context.respond(
                f"Contribution of {project_contribution} pending review by project manager",
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            # Create contribution review message
            await ContributionReviewView.new(context, project_contribution, self.view)

        self.view.session.commit()

        await self.view.update_message()


# Accept/reject contribution


class ContributionReviewView(miru.View):
    @classmethod
    async def new(
        cls,
        context: miru.ModalContext,
        project_contribution: ProjectContribution,
        project_view: ProjectView,
    ) -> "Self":
        """Create a new contribution review, ensuring message is sent and link to it persisted."""

        view = cls(project_contribution, project_view)

        reviewer = await context.bot.rest.fetch_user(project_contribution.reviewer_id)
        message = await reviewer.send(
            view.generate_message(),
            components=view,
            flags=hikari.MessageFlag.SUPPRESS_EMBEDS,
        )
        await view.start(message)

        project_contribution.review_channel_id = message.channel_id
        project_contribution.review_message_id = message.id

    def __init__(
        self, project_contribution: ProjectContribution, project_view: ProjectView
    ) -> None:
        super().__init__(timeout=None)

        self.project_contribution = project_contribution
        self.project_view = project_view

        self.add_item(
            AcceptButton(
                self.project_view.project.creator_id,
                str(self.project_contribution.project_contribution_id),
            )
        )
        self.add_item(
            RejectButton(
                self.project_view.project.creator_id,
                str(self.project_contribution.project_contribution_id),
            )
        )
        self.add_item(
            CounterOfferButton(str(self.project_contribution.project_contribution_id))
        )

    def generate_message(self) -> str:
        """Generate the message for this view."""

        return (
            f"{self.project_contribution.contributor_name} has made an offer for "
            f"{self.project_view.project} {self.project_view.message_link}:\n"
            f"{self.project_contribution}"
        )

    async def get_message(self) -> hikari.Message:
        """Get message, reloading if needed"""

        if self._message is None:
            assert self.project_contribution.review_channel_id is not None
            assert self.project_contribution.review_message_id is not None
            self._message = await self.app.rest.fetch_message(
                self.project_contribution.review_channel_id,
                self.project_contribution.review_message_id,
            )

        return self._message

    async def finish(self) -> None:
        """Complete this view and close it."""

        message = await self.get_message()
        await message.edit(components=None)
        self.stop()


class AcceptButton(miru.Button):
    """Button that allows for accepting an offer."""

    view: Union[ContributionReviewView, "CounterOfferReviewView"]

    def __init__(
        self, notify_user_id: hikari.Snowflake, project_contribution_id: str
    ) -> None:
        super().__init__(
            style=hikari.ButtonStyle.PRIMARY,
            label="Accept",
            custom_id=f"accept_contribution_offer_{project_contribution_id}",
        )

        self.notify_user_id = notify_user_id

    async def callback(self, context: miru.ViewContext) -> None:
        """Accept the contribution and update the project."""

        # Accept contribution
        self.view.project_contribution.accepted = True
        self.view.project_view.session.commit()

        # Notify contributor or project owner (in case of counter offer)
        notify_user = await context.app.rest.fetch_user(self.notify_user_id)
        try:
            await notify_user.send(
                f"Your contribution offer of {self.view.project_contribution} "
                f"for {self.view.project_view.project} {self.view.project_view.message_link} "
                "has been **accepted**",
                flags=hikari.MessageFlag.SUPPRESS_EMBEDS,
            )
        except ForbiddenError:
            LOGGER.warning(
                f"Unable to notify {notify_user.global_name} about "
                f"{self.view.project_contribution} being accepted"
            )

        # Update project view
        await self.view.project_view.update_message()
        await self.view.finish()


class RejectButton(miru.Button):
    """Button that allows for rejecting an offer"""

    view: Union[ContributionReviewView, "CounterOfferReviewView"]

    def __init__(
        self,
        notify_user_id: hikari.Snowflake,
        project_contribution_id: str,
    ) -> None:
        super().__init__(
            style=hikari.ButtonStyle.DANGER,
            label="Reject",
            custom_id=f"reject_contribution_offer_{project_contribution_id}",
        )

        self.notify_user_id = notify_user_id

    async def callback(self, context: miru.ViewContext) -> None:
        """Accept the contribution and update the project."""

        # Reject contribution
        self.view.project_contribution.accepted = False
        self.view.project_view.session.commit()

        # Notify contributor or project owner (in case of counter offer)
        notify_user = await context.app.rest.fetch_user(self.notify_user_id)
        await notify_user.send(
            f"Your contribution offer of {self.view.project_contribution} "
            f"for {self.view.project_view.project} {self.view.project_view.message_link} "
            "has been **rejected**",
            flags=hikari.MessageFlag.SUPPRESS_EMBEDS,
        )
        await self.view.project_view.update_message()
        await self.view.finish()


# Counter offers
class CounterOfferButton(miru.Button):
    """Button that allows for sending a counter offer"""

    view: ContributionReviewView

    def __init__(self, project_contribution_id: str) -> None:
        super().__init__(
            style=hikari.ButtonStyle.SECONDARY,
            label="CounterOffer",
            custom_id=f"counter_offer_{project_contribution_id}",
        )

    async def callback(self, context: miru.ViewContext) -> None:
        """Accept the contribution and update the project."""

        # Send modal for counteroffer
        modal = CounterOfferModal(
            self.view.project_view, self.view.project_contribution, self.view
        )

        await modal.send(context.interaction)
        await modal.wait()


class CounterOfferModal(miru.Modal):
    """Modal that prompts for info for a counter offer."""

    def __init__(
        self,
        project_view: ProjectView,
        contribution: ProjectContribution,
        contribution_view: ContributionReviewView,
    ) -> None:
        super().__init__(f"Counter offer for {contribution.project_item.name}")

        self.view = project_view
        self.contribution_view = contribution_view
        self.contribution = contribution

        self.amount = miru.TextInput(
            label="Amount", value=str(contribution.amount), required=True
        )
        self.price_per_item = miru.TextInput(
            label="Price Per Item",
            value=str(contribution.price_per_item),
            required=True,
        )
        self.currency = miru.TextInput(
            label="Currency", value=contribution.currency, required=True
        )

        self.add_item(self.amount)
        self.add_item(self.price_per_item)
        self.add_item(self.currency)

    async def callback(self, context: miru.ModalContext) -> None:
        """Create contribution, auto accept if price matches the offer"""
        assert self.amount.value is not None
        assert self.price_per_item.value is not None
        assert self.currency.value is not None

        try:
            project_contribution = create_project_contribution(
                self.view.session,
                self.contribution.project_item,
                self.contribution.contributor_id,
                self.contribution.contributor_name,
                self.amount.value,
                self.price_per_item.value,
                self.currency.value,
            )
        except ValidationError as error:
            await context.respond(
                content=(str(error)),
                flags=hikari.MessageFlag.EPHEMERAL,
            )
            return

        if (
            project_contribution.amount
            > self.contribution.project_item.remaining_items_needed
        ):
            project_contribution.amount = (
                self.contribution.project_item.remaining_items_needed
            )

        project_contribution.is_counter_offer = True

        self.contribution.accepted = False
        self.view.session.commit()

        await self.contribution_view.finish()

        await context.respond(
            f"Counter offer of {project_contribution} pending review by contributor",
            flags=hikari.MessageFlag.EPHEMERAL,
        )
        # Create contribution review message
        await CounterOfferReviewView.new(context, project_contribution, self.view)
        self.view.session.commit()

        await self.view.update_message()


class CounterOfferReviewView(miru.View):
    @classmethod
    async def new(
        cls,
        context: miru.ModalContext,
        project_contribution: ProjectContribution,
        project_view: ProjectView,
    ) -> "Self":
        """Create a new contribution review for a contributor to review a counter offer."""

        view = cls(project_contribution, project_view)

        reviewer = await context.bot.rest.fetch_user(
            project_contribution.contributor_id
        )
        message = await reviewer.send(
            view.generate_message(),
            components=view,
            flags=hikari.MessageFlag.SUPPRESS_EMBEDS,
        )
        await view.start(message)

        project_contribution.review_channel_id = message.channel_id
        project_contribution.review_message_id = message.id

    def __init__(
        self,
        project_contribution: ProjectContribution,
        project_view: ProjectView,
    ) -> None:
        super().__init__(timeout=None)

        self.project_contribution = project_contribution
        self.project_view = project_view

        self.add_item(
            AcceptButton(
                self.project_view.project.creator_id,
                str(self.project_contribution.project_contribution_id),
            )
        )
        self.add_item(
            RejectButton(
                self.project_view.project.creator_id,
                str(self.project_contribution.project_contribution_id),
            )
        )

    def generate_message(self) -> str:
        """Generate the message for this view."""

        return (
            f"{self.project_contribution.project_item.project.creator_name} has made a counter "
            f"offer for {self.project_view.project} {self.project_view.message_link}:\n"
            f"{self.project_contribution}"
        )

    async def get_message(self) -> hikari.Message:
        """Get message, reloading if needed"""

        if self._message is None:
            assert self.project_contribution.review_channel_id is not None
            assert self.project_contribution.review_message_id is not None
            self._message = await self.app.rest.fetch_message(
                self.project_contribution.review_channel_id,
                self.project_contribution.review_message_id,
            )

        return self._message

    async def finish(self) -> None:
        """Complete this view and close it."""

        message = await self.get_message()
        await message.edit(components=None)
        self.stop()


# Reload views on startup
@bot.listen()
async def startup_views(_event: hikari.StartedEvent) -> None:
    """Load up project views from persistence on startup."""

    with DatabaseSession() as session:
        for project in get_projects(session):
            if project.message_id is None or project.channel_id is None:
                LOGGER.warning(f"Unable to load project view for {project}")
                continue

            project_view = ProjectView(project.project_id)
            await project_view.start(project.message_id)

            for contribution in project_view.project.contributions:
                if contribution.accepted is not None:
                    continue

                if (
                    contribution.review_message_id is None
                    or contribution.review_channel_id is None
                ):
                    LOGGER.warning(
                        f"Cannot reload review view for project contribution: {contribution}"
                    )
                    continue

                if contribution.is_counter_offer:
                    review_view = CounterOfferReviewView(contribution, project_view)
                else:
                    review_view = ContributionReviewView(contribution, project_view)

                await review_view.start(contribution.review_message_id)


# Persistable Context utility


class ProjectContext(miru.Context):
    """Magic hax. Do not attempt at home.

    This magically tricks the rest of Hikari and Miru to work with the message that a
    project is associated with.
    """

    interaction: "ProjectInteraction"

    def __init__(self, project: Project) -> None:
        """Create a context attached to the particular project."""

        self.project = project
        super().__init__(ProjectInteraction(project))
        # All projects already have a response
        self._issued_response = True

    @property
    def initialized(self) -> bool:
        """Check if the context has been initialized or not"""

        return self.interaction.initialized

    async def init(self) -> None:
        """Initialize the context"""

        await self.interaction.init()


class ProjectInteraction(hikari.interactions.MessageResponseMixin):
    """An interaction with a project."""

    def __init__(self, project: Project) -> None:
        """Create an interaction attached to the particular project."""

        self.project = project
        self.app: lightbulb.BotApp = bot

    async def init(self):
        """Initialize the interaction"""

        if not self.initialized:
            self.app.application = await self.app.rest.fetch_application()

    @property
    def initialized(self) -> bool:
        """Check if the context has been initialized or not"""

        return self.app.application is not None

    @property
    def application_id(self) -> hikari.Snowflake:
        """Get the application id for the app being used."""
        assert bot.application is not None
        return bot.application.id

    async def edit_initial_response(
        self,
        content: undefined.UndefinedNoneOr[Any] = undefined.UNDEFINED,
        **kwargs: Any,
    ) -> messages.Message:
        """Update the initial message for the project."""

        assert self.project.channel_id is not None
        assert self.project.message_id is not None
        message = await self.app.rest.fetch_message(
            self.project.channel_id, self.project.message_id
        )

        return await message.edit(content, **kwargs)
