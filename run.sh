source ./secrets.sh


# Take a backup of the database before running migration
touch pu_bot.db
cp pu_bot.db backup.db

# Run alembic migration
poetry run alembic upgrade head

poetry run python -m pu_bot