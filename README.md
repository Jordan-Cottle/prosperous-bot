# Prosperous Bot

This is a bot purpose built for helping members of a prosperous universe discord server coordinate
sales and requests of goods.

[Invite me!](https://discord.com/api/oauth2/authorize?client_id=1084316099086127105&permissions=2048&scope=bot)


## Commands

`/auction` with helps coordinate fairly distributing goods to players who want them. It lets you say
"Looking to sell a bunch of X for Y price per unit" and people can sign up for a slice of the total
amount. The bot does all the math and spits out the numbers you need to type into contracts.

`/order` is like the inverse of an auction. You ask for a volume of goods at a set price and people
can contribute all or some of it. Very useful for asking for large volumes of things like building
materials, fuel, consumables or generally hard to find items like premium ship components
that might be produced on demand.

`/project` is a new command I recently added that is like `/order` but for many things at the same
time. It also includes a way for contributors to negotiate prices if the one set by the requestor is
too low/high. This is particularly handy for things like coordinating ship and base construction
projects.

`/shipping-offer` is for when you have spare cargo space and want to coordinate filling it up. You
can set how much weight/volume is available, where you are starting and heading to, when you're
planning on leaving, and even a price per weight/volume. Then anyone who wants to add some items to
tag along can submit items and the bot tracks how much space is available as well as the expected
"tip" for that LM order or contract.