"""Models for coordinating shipping"""

from datetime import datetime
from typing import List, Optional
from uuid import UUID, uuid4

from hikari import Snowflake
from sqlmodel import Field, Relationship, SQLModel

from ..interfaces.fio import ALL_MATERIALS


class OpenShippingCapacityOffer(SQLModel, table=True):
    """An open offer for shipping capacity from one location to another"""

    open_shipping_capacity_offer_id: UUID = Field(
        default_factory=uuid4, primary_key=True
    )

    origin: str
    destination: str

    total_weight_available: float
    total_volume_available: float
    shipping_rate: float
    currency: str
    launches_at: Optional[datetime]

    created_on: datetime = Field(default_factory=datetime.now)
    finished_at: Optional[datetime] = None

    message_id: Optional[Snowflake] = None
    channel_id: Optional[Snowflake] = None
    guild_id: Snowflake

    creator_id: Snowflake
    creator_name: str

    shipments: List["OpenOfferShipment"] = Relationship(back_populates="open_offer")

    @property
    def free_weight(self) -> float:
        """Get the free weight remaining."""
        total = self.total_weight_available
        for shipment in self.shipments:
            total -= shipment.weight

        return total

    @property
    def free_volume(self) -> float:
        """Get the free volume remaining."""
        total = self.total_volume_available
        for shipment in self.shipments:
            total -= shipment.volume

        return total


class OpenOfferShipment(SQLModel, table=True):
    """A volume of goods to be sent as part of a shipping offer"""

    shipment_id: UUID = Field(default_factory=uuid4, primary_key=True)
    open_shipping_capacity_offer_id: UUID = Field(
        foreign_key="openshippingcapacityoffer.open_shipping_capacity_offer_id"
    )

    user_id: Snowflake
    user_name: str

    item: str
    amount: int

    open_offer: OpenShippingCapacityOffer = Relationship(back_populates="shipments")

    @property
    def weight(self) -> float:
        """Get the weight of the shipment."""
        weight_per_item = ALL_MATERIALS[self.item].weight

        return self.amount * weight_per_item

    @property
    def volume(self) -> float:
        """Get the volume of the shipment."""
        volume_per_item = ALL_MATERIALS[self.item].volume

        return self.amount * volume_per_item

    @property
    def price(self) -> float:
        """The cost to ship this shipment based on offer price"""

        return max(self.weight, self.volume) * self.open_offer.shipping_rate
