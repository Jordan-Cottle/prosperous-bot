"""Set up the core bot for other modules to import. """
import logging

import lightbulb
import miru
from pydantic import BaseSettings

LOGGER = logging.getLogger("pu_bot")


class BotSettings(BaseSettings, env_prefix="PU_BOT_"):
    """Settings for configuring the bot."""

    token: str
    prefix: str = "pu"
    logs: str = "DEBUG"
    savefile: str = "auction_list.json"


SETTINGS = BotSettings()  # type: ignore

bot: lightbulb.BotApp = lightbulb.BotApp(
    token=SETTINGS.token,
    prefix=SETTINGS.prefix,
    logs=SETTINGS.logs,
)


miru.install(bot)  # type: ignore
