"""Module for logic that watches for expiring audits and closes them out."""

import asyncio
from datetime import datetime, timedelta, timezone
import logging

from hikari import ForbiddenError
from pu_bot import bot
from pu_bot.commands.auctions import AUCTION_VIEWS, create_message_link
from pu_bot.database.auction import get_auctions
from pu_bot.database.session import DatabaseSession
from pu_bot.models.auction import Auction

LOGGER = logging.getLogger(__name__)


async def audit_auctions() -> None:
    """Audit auctions to ensure that old ones are closed in a timely manner"""

    now = datetime.now(tz=timezone.utc)
    with DatabaseSession() as session:
        auctions = get_auctions(session)

        for auction in auctions:
            if auction.created_at is None:
                LOGGER.debug("Skipping auction that hasn't been posted")
                continue

            end_time = auction.created_at + timedelta(days=auction.max_duration_days)

            if now >= end_time:
                overdue_days = (now - end_time).days
                LOGGER.info(
                    f"Found an expired auction that is {overdue_days} days over due"
                )

                try:
                    await close_expired_auction(auction)
                except ForbiddenError as error:
                    LOGGER.warning(
                        f"Couldn't close expired auction from {auction.creator_name} due to {error}"
                    )
                    auction.complete = True
                    continue


async def close_expired_auction(auction: Auction) -> None:
    """Close an expired auction and notify the user."""
    auction_view = AUCTION_VIEWS[auction.auction_id]

    await auction_view.complete_auction()

    auction_creator = await bot.rest.fetch_user(auction.creator_id)
    assert auction.created_at is not None
    assert auction.channel_id is not None
    assert auction.message_id is not None

    days = (datetime.now(tz=timezone.utc) - auction.created_at).days
    message = (
        f"<@{auction.creator_id}>, your auction has automatically expired after {days} days: "
        f"{create_message_link(auction.guild_id, auction.channel_id, auction.message_id)}"
    )
    LOGGER.info(f"Sending auction expired message to {auction_creator.global_name}")
    # await bot.rest.create_message(auction.channel_id, message, user_mentions=True)
    try:
        await auction_creator.send(message)
    except ForbiddenError:
        LOGGER.warning(
            f"Unable to notify {auction_creator.global_name} about {auction} expiring"
        )


async def audit_auctions_forever() -> None:
    """Loop forever checking for and closing old auctions."""
    while True:
        try:
            await audit_auctions()
        except Exception as error:
            LOGGER.exception(f"Auction audit failed: {error!r}")

        await asyncio.sleep(3600)
