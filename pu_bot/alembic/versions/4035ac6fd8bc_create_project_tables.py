"""create project tables

Revision ID: 4035ac6fd8bc
Revises: b61a60f4bf7e
Create Date: 2024-05-08 22:23:25.423776

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
import sqlmodel


# revision identifiers, used by Alembic.
revision: str = "4035ac6fd8bc"
down_revision: Union[str, None] = "b61a60f4bf7e"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "project",
        sa.Column("project_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("preferred_location", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("guild_id", sa.Integer(), nullable=True),
        sa.Column("message_id", sa.Integer(), nullable=True),
        sa.Column("channel_id", sa.Integer(), nullable=True),
        sa.Column("creator_id", sa.Integer(), nullable=False),
        sa.Column("creator_name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("created_on", sa.DateTime(), nullable=False),
        sa.Column("completed_on", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("project_id"),
    )
    op.create_table(
        "projecttemplate",
        sa.Column("project_template_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("preferred_location", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("guild_id", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("project_template_id"),
    )
    op.create_table(
        "projectitem",
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("total_amount", sa.Integer(), nullable=False),
        sa.Column("price_per_item", sa.Float(), nullable=True),
        sa.Column("preferred_currency", sqlmodel.sql.sqltypes.AutoString(), nullable=True),
        sa.Column("project_item_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("project_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.ForeignKeyConstraint(
            ["project_id"],
            ["project.project_id"],
        ),
        sa.PrimaryKeyConstraint("project_item_id"),
    )
    op.create_table(
        "projecttemplateitem",
        sa.Column("project_template_item_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("project_template_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("total_amount", sa.Integer(), nullable=False),
        sa.Column("price_per_item", sa.Float(), nullable=True),
        sa.Column("preferred_currency", sqlmodel.sql.sqltypes.AutoString(), nullable=True),
        sa.ForeignKeyConstraint(
            ["project_template_id"],
            ["projecttemplate.project_template_id"],
        ),
        sa.PrimaryKeyConstraint("project_template_item_id", "project_template_id"),
    )
    op.create_table(
        "projectcontribution",
        sa.Column("project_contribution_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("project_item_id", sqlmodel.sql.sqltypes.GUID(), nullable=False),
        sa.Column("contributor_id", sa.Integer(), nullable=False),
        sa.Column("contributor_name", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("amount", sa.Integer(), nullable=False),
        sa.Column("price_per_item", sa.Float(), nullable=False),
        sa.Column("currency", sqlmodel.sql.sqltypes.AutoString(), nullable=False),
        sa.Column("review_channel_id", sa.Integer(), nullable=True),
        sa.Column("review_message_id", sa.Integer(), nullable=True),
        sa.Column("is_counter_offer", sa.Boolean(), nullable=False),
        sa.Column("accepted", sa.Boolean(), nullable=True),
        sa.ForeignKeyConstraint(
            ["project_item_id"],
            ["projectitem.project_item_id"],
        ),
        sa.PrimaryKeyConstraint("project_contribution_id"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("projectcontribution")
    op.drop_table("projecttemplateitem")
    op.drop_table("projectitem")
    op.drop_table("projecttemplate")
    op.drop_table("project")
    # ### end Alembic commands ###
