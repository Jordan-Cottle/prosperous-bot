"""Tests for the models/project.py module"""

from typing import Type
import pytest

from pu_bot.models.project import ItemRequest


@pytest.mark.parametrize(
    "input_str, expected_item",
    [
        ("100RAT", ItemRequest(name="RAT", total_amount=100)),
        ("100xRAT", ItemRequest(name="RAT", total_amount=100)),
        ("100 RAT", ItemRequest(name="RAT", total_amount=100)),
        (
            "100 RAT@200AIC/u",
            ItemRequest(
                name="RAT",
                total_amount=100,
                price_per_item=200,
                preferred_currency="AIC",
            ),
        ),
        (
            "100 RAT@200 AIC/u",
            ItemRequest(
                name="RAT",
                total_amount=100,
                price_per_item=200,
                preferred_currency="AIC",
            ),
        ),
        (
            "100 RAT @ 200 AIC/u",
            ItemRequest(
                name="RAT",
                total_amount=100,
                price_per_item=200,
                preferred_currency="AIC",
            ),
        ),
        (
            "100xxRAT @  200AIC/u",
            ItemRequest(
                name="RAT",
                total_amount=100,
                price_per_item=200,
                preferred_currency="AIC",
            ),
        ),
        (
            "100  RAT@200.5 AIC/u",
            ItemRequest(
                name="RAT",
                total_amount=100,
                price_per_item=200.5,
                preferred_currency="AIC",
            ),
        ),
        (
            "100O",
            ItemRequest(
                name="O",
                total_amount=100,
            ),
        ),
    ],
)
def test_item_request_parsing(input_str: str, expected_item: ItemRequest) -> None:
    """Verify that items parse as expected."""

    item = ItemRequest.parse_input(input_str)

    assert item == expected_item


@pytest.mark.parametrize(
    "input_str",
    [
        "100",
        "RAT",
    ],
)
def test_item_parsing(input_str: str) -> None:
    """Verify that items parse as expected."""

    with pytest.raises(
        ValueError, match=f"Unable to parse '{input_str}' as a valid item request"
    ):
        ItemRequest.parse_input(input_str)
