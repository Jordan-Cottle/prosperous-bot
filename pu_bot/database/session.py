import logging
from typing import Any, Union

from pydantic import BaseSettings, validator
from sqlalchemy.engine import Connection, Engine
from sqlmodel import Session, SQLModel, create_engine

LOGGER = logging.getLogger(__name__)


class DatabaseSettings(BaseSettings, env_prefix="database_"):
    """Settings for configuring the database"""

    url: str = "sqlite:///:memory:"
    initialize_schema: bool = False
    echo: bool = False

    @validator("initialize_schema", always=True)
    def set_if_in_memory_db(cls, value, values):
        """Set initialize schema automatically if using an in-memory database."""

        if ":memory:" in values.get("url", ""):
            LOGGER.info("Automatically creating DB schema for in memory database")
            return True

        return value


DATABASE_SETTINGS = DatabaseSettings()
ENGINE = create_engine(DATABASE_SETTINGS.url, echo=DATABASE_SETTINGS.echo)


class DatabaseSession(Session):
    def __init__(
        self, bind: Union[Connection, Engine] = ENGINE, *args: Any, **kwargs: Any
    ) -> None:
        super().__init__(bind, *args, **kwargs)

    def __enter__(self: "DatabaseSession") -> "DatabaseSession":
        return super().__enter__()

    def __exit__(self, exc_type: Any, exc_value: Any, exc_traceback: Any) -> None:
        if exc_type or exc_value or exc_traceback:
            self.rollback()
        else:
            self.commit()

        self.close()


if DATABASE_SETTINGS.initialize_schema:
    # Initialize models directory so all models are registered in the meta data
    from ..models import *

    SQLModel.metadata.create_all(ENGINE)
