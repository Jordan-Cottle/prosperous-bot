"""Interface for interacting with the FIO api"""

from datetime import datetime
from typing import Dict, List
from pydantic import BaseModel, Field

from httpx import AsyncClient

# Dirty material cache/index
ALL_MATERIALS: Dict[str, "Material"] = {}


class Material(BaseModel):
    """Information about a material in PU"""

    material_id: str = Field(alias="MaterialId")
    category_name: str = Field(alias="CategoryName")
    category_id: str = Field(alias="CategoryId")
    name: str = Field(alias="Name")
    ticker: str = Field(alias="Ticker")
    weight: float = Field(alias="Weight")
    volume: float = Field(alias="Volume")
    user_name_submitted: str = Field(alias="UserNameSubmitted")
    timestamp: datetime = Field(alias="Timestamp")


async def get_materials() -> Dict[str, Material]:
    """Get all materials in the game"""

    if ALL_MATERIALS:
        return ALL_MATERIALS

    async with AsyncClient() as client:
        response = await client.get("https://rest.fnar.net/material/allmaterials")
        response.raise_for_status()
        materials = response.json()

    all_materials = [Material.parse_obj(material) for material in materials]

    for material in all_materials:
        ALL_MATERIALS[material.ticker] = material
        ALL_MATERIALS[material.name] = material

    return ALL_MATERIALS
